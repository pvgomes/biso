<?php

namespace Tests\VO;

use Domain;


class PriceTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function copiedMoneyShouldRepresentSameValue()
    {
        $aMoney = new Domain\VO\Money(100, new Domain\VO\Currency('USD'));
        $copiedMoney = Domain\VO\Money::fromMoney($aMoney);
        $this->assertTrue($aMoney->equals($copiedMoney));
    }

}

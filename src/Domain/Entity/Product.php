<?php

namespace Domain\Entity;

use Domain\VO;

class Product
{

    private $id;
    private $name;
    private $price;
    private $measure;
    private $category;
    private $specification;


    public function __construct(Category $category,
                                VO\Specification $specification,
                                VO\Price $price,
                                VO\Measure $measure,
                                $id, $name)
    {
        $this->category = $category;
        $this->id = $id;
        $this->measure = $measure;
        $this->price = $price;
        $this->specification = $specification;
        $this->name = $name;
    }

    /**
     * @return Category
     */
    public function category()
    {
        return $this->category;
    }

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return VO\Measure
     */
    public function measure()
    {
        return $this->measure;
    }

    /**
     * @return mixed
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return VO\Price
     */
    public function price()
    {
        return $this->price;
    }

    /**
     * @return VO\Specification
     */
    public function specification()
    {
        return $this->specification;
    }



}